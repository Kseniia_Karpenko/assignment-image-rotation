#include <stdbool.h>
#include <stdio.h>

#include "bmp.h"
#include "util.h"


void usage() {
    fprintf(stderr, "Usage: ./rotate_image BMP_FILE_NAME\n"); 
}

int main(int argc, char** argv) {
    if (argc != 2) {
	usage();
    }
    if (argc < 2) {
	err("Not enough arguments \n");
    }
    if (argc > 2) {
	err("Too many arguments \n");
    }

    FILE *file = NULL;
    struct image img;
    if ((file = open_file_read(argv[1])) == NULL) {
	err("Ошибка открытие файла для чтения\n");
    } else {
	fprintf(stderr, "Файл для чтения успешно открыт\n");
    }
    
    enum read_status result;
    result = from_bmp(file, &img);

    check_read_result(result);
    
    img = rotate(img);
    if (close_file(file) != CLOSE_OK) {
        err("Ошибка закрытия файла\n");
    } else {
	fprintf(stderr, "Файл для чтения успешно закрыт\n");  
    }
    
    if ((file = open_file_write(argv[1])) == NULL) {
        err("Ошибка открытия файла для записи\n");
    } else {
	fprintf(stderr, "Файл для записи успешно открыт\n");  
    }
    enum write_status write_result;
    write_result = to_bmp(file, &img);
    free(img.data);
    
    if (write_result == WRITE_ERROR) {
	err("Ошибка записи файла\n");
    } else {
	fprintf(stderr, "Запись произошла успешно\n");
    }
    if (close_file(file) != CLOSE_OK) {
        err("Ошибка закрытия файла\n");
    } else {
	fprintf(stderr, "Файл для записи успешно закрыт\nВсё прошло успешно\n");
    }
    return 0;
}
