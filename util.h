#ifndef _UTIL_H_
#define _UTIL_H_
#include <stdio.h>

_Noreturn void err(const char* msg, ...);

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_VERSION,
    READ_UNEXPECTED_EOF,
    READ_FILE_ERROR
    /* коды других ошибок  */
    };
    
/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

/*  Открытие файла   */
enum  open_status  {
    OPEN_OK = 0,
    OPEN_READ_ERROR,
    OPEN_WRITE_ERROR
    /* коды других ошибок  */
};

/*  Закрытие файла  */
enum  close_status  {
    CLOSE_OK = 0,
    CLOSE_ERROR
    /* коды других ошибок  */
};

void check_read_result(enum read_status result);

FILE* open_file_read(const char* path);

FILE* open_file_write(const char* path);

enum close_status close_file(FILE* out);

#endif
