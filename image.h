#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct pixel {uint8_t b, g, r;};

struct image rotate(struct image const source);

#endif
