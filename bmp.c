#include "bmp.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>

#define BMP_HEADER_SIZE 54
#define BMP_FILE_SIGNATURE 0x4D42

struct  __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

struct bmp_header create_bmp_header(uint64_t width, uint64_t height){
    return (struct bmp_header) {
            .bfType = BMP_FILE_SIGNATURE,
            .bfileSize = width*height*24,
            .bfReserved = 0,
            .bOffBits = BMP_HEADER_SIZE,
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = sizeof(struct bmp_header) + width*height*24,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

static bool read_header(FILE* f, struct bmp_header* header) {
    return fread(header, sizeof(struct bmp_header), 1, f);
}

enum read_status from_bmp(FILE* in, struct image* img)
{
    struct bmp_header header = {0};
    if (!read_header(in, &header)) {
	err("Failed to open BMP file or reading header.\n");
        }

    if (header.bfType != BMP_FILE_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }
    
    // Проверяем глубину цвета (поддерживается только 24 бита на пиксель)
    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    // Проверяем версию BMP (поддерживаются только заголовки BITMAPINFOHEADER)
    if (header.bOffBits != BMP_HEADER_SIZE) {
        return READ_INVALID_VERSION;      
    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    const uint64_t size = img->width * img->height;

    img->data = (struct pixel*) malloc(size * sizeof(struct pixel));

    
    /* Переходим к считыванию цветов пикселей */
    fseek(in, BMP_HEADER_SIZE, SEEK_SET);

    /* Считываем значения цветовых компонентов, преобразуя к виду
       с плавающей точкой */
    for (uint64_t i = 0; i < img->height; i++) {
	if (fread(&(img->data[i * img->width]), sizeof (struct pixel), img->width, in) < 1) {
            free(img->data);
            if (feof(in)) {
		return READ_UNEXPECTED_EOF;
	    }
            return READ_FILE_ERROR;
        }

        
        // Пропускаем байты padding
        if (img->width * 3 % 4 != 0) {
	    fseek(in, 4 - img->width * 3 % 4, SEEK_CUR);
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img)
{

     const uint8_t padding_byte = 0;
     
    struct bmp_header header = create_bmp_header(img->width, img->height);

    fwrite(&header, sizeof(struct bmp_header), 1, out);
    
    /* Переходим к считыванию цветов пикселей */
    if (fseek(out, BMP_HEADER_SIZE, SEEK_SET) != 0) {
	return WRITE_ERROR;
    }

    /* Записываем значения цветовых компонентов, преобразуя к виду
       с плавающей точкой */
    for(uint64_t i = 0; i < img->height; i++) {
	fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out);
        
        // Добавляем байты padding
        if (img->width * 3 % 4 != 0) {
	    fwrite(&padding_byte, 1, 4 - img->width * 3 % 4, out);
        }
    }
    return WRITE_OK;
}
