#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "util.h"

_Noreturn void err(const char* msg, ...) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  va_end (args);
  exit(1);
}

FILE* open_file_read(const char* path)
{
    return fopen(path, "r");
}

FILE* open_file_write(const char* path)
{
    return fopen(path, "w");
}

enum close_status close_file(FILE* file)
{
    if (fclose(file) != EOF) {
        return CLOSE_OK;
    }
    return CLOSE_ERROR;
} 

void check_read_result(enum read_status result) {
    static char const* const messages[] = {
	[READ_INVALID_BITS] = "Wrong count of bits",
	[READ_INVALID_HEADER] = "File is too short to be a BMP file",
	[READ_INVALID_SIGNATURE] = "BMP signature doesn't match",
	[READ_INVALID_VERSION] = "Unsupported BMP version",
	[READ_UNEXPECTED_EOF] = "Unexpected end of file",
	[READ_FILE_ERROR] = "FILE ERROR",
	[READ_OK] = "Успешно считали изображение во внутренний формат"
    };
    
    if(result != READ_OK)
    {
	err("%s\n",messages[result]);
    }
    fprintf(stderr, "%s\n", messages[result]);
}
