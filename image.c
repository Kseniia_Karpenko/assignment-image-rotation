#include "image.h"

struct image rotate(struct image const source)
{
    struct image newImage = {
	.height = source.width,
	.width = source.height,
    };
    
    newImage.data = (struct pixel*) malloc(newImage.width * newImage.height * sizeof(struct pixel));
    
    for (uint64_t i = 0; i < newImage.height; i++) {
	for (uint64_t k = 0; k < newImage.width; k++) {
	    newImage.data[i * newImage.width + k] = source.data[(newImage.width - k - 1) * newImage.height + i];
	}
    }
    return newImage;
}
